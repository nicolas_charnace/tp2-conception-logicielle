# Lancement du hello world
Executer hello_world.py
` python3 hello_world.py `

# Lancement du ratio de la différence entre des chaines de caractères

Package nécessaire : fuzzywuzzy; Levenshtein
Executer fuzz.py
```
pip install fuzzywuzzy
pip install Levenshtein
python3 fuzz.py
```

# Lancement du ratio de la différence entre des chaines de caractères

Package nécessaire : uvicorn; fastapi
Executer main.py
```
pip install uvicorn
pip install fastapi
python3 main.py
```
